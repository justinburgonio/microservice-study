from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO

# Declare a function to update the AccountVO object (ch, method, properties, body)
def account_update(ch, menthod, properties, body):
#   content = load the json in body
    content = json.loads(body)
#   first_name = content["first_name"]
    first_name = content["first_name"]
#   last_name = content["last_name"]
    last_name = content["last_name"]
#   email = content["email"]
    email = content["email"]
#   is_active = content["is_active"]
    is_active = content["is_active"]
#   updated_string = content["updated"]
    updated_string = content["updated"]
#   updated = convert updated_string from ISO string to datetime
    updated = datetime.fromisoformat(updated_string)
#   if is_active:
    if is_active:
#       Use the update_or_create method of the AccountVO.objects QuerySet
        AccountVO.objects.update_or_create(
#           to update or create the AccountVO object
            {
                "first_name": first_name,
                "last_name": last_name,
                "is_active": is_active,
                "updated": updated,
                "email": email
            }
        )
#   otherwise:
    else:
#       Delete the AccountVO object with the specified email, if it exists
        AccountVO.email.delete()



# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
# infinite loop
while True:
    try:
#       create the pika connection parameters
        parameters = pika.ConnectionParameters(host="rabbitmq")
#       create a blocking connection with the parameters
        connection = pika.BlockingConnection(parameters)
#       open a channel
        channel = connection.channel()
#       declare a fanout exchange named "account_info"
        channel.exchange_declare(exchange="account_info", exchange_type="fanout")
        result = channel.queue_declare(queue="", exclusive=True)
#       declare a randomly-named queue
        queue_name = result.method.queue
#       get the queue name of the randomly-named queue
        print(queue_name)
#       bind the queue to the "account_info" exchange
        channel.queue_bind(exchange="account_info", queue=queue_name)

#       do a basic_consume for the queue name that calls
#           function above
        channel.basic_consume(
            queue="", on_message_callback=account_update, auto_ack=True
        )
#       tell the channel to start consuming
        channel.start_consuming()
#   except AMQPConnectionError
    except AMQPConnectionError:
#       print that it could not connect to RabbitMQ
        print("Could not connect to RabbitMQ")
#       have it sleep for a couple of seconds
        time.sleep(2.0)
